<?php
include_once 'connect_db.php';

function getFields() {
    $db = sql_query('SELECT * FROM field');
    $res = [];
    while ($row = $db->fetch_assoc()) {
        $res[] = $row['title'];
    }
    return $res;
}

function getHashtags() {
    $db = sql_query('SELECT * FROM hashtags');
    $res = [];
    while ($row = $db->fetch_assoc()) {
        $res[] = $row['name'];
    }
    return $res;
}

function getFieldAndHashtags() {
    $res = sql_query('SELECT * FROM FieldAndHashtags');
    return $res;
}
?>
