<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles/style.css">
    <title>Добавление # к областям знаний</title>
  </head>
  <body>
    <?php
      include 'php/get_data.php'
    ?>

    <form action="php/form.php" class="form" method="post">
      <label for="input" class="form__label">Хештег</label>
      <input type="text" name="h_name" class="form__input form__input-type-text" id="input">
      <select class="form__select form__input form__input-type-select" name="f_name">
        <option value="Кулинария" class="form__opt">Кулинария</option>
        <option value="Разное" class="form__opt">Разное</option>
      </select>
      <input class="form__input form__input-btn" type="submit" name="" value="Добавить">
    </form>



    <div class="main">


      <div class="main__column column column-left">
        <p class="column__title">Кулинария</p>
        <ul class="column__list column__list-f1">
          <?php
            $FieldAndHashtags = getFieldAndHashtags();
            while($row = $FieldAndHashtags->fetch_assoc()) {
              if ($row['f_id'] == 1) {
                $hashtag = sql_query('SELECT h_name FROM `std_1720_backend_php`.`hashtags` WHERE `h_id` =' . $row['h_id'])->fetch_assoc()['h_name'];
              ?>
              <li class="column__h1">
                <?php echo $hashtag ?>
              </li>
              <?php
              }
            }
          ?>
        </ul>
      </div>

      <div class="main__column column column-right">
        <p class="column__title">Разное</p>
        <ul class="column__list column__list-f2">
          <?php
            $FieldAndHashtags = getFieldAndHashtags();
            while($row = $FieldAndHashtags->fetch_assoc()) {
              if ($row['f_id'] == 2) {
                $hashtag = sql_query('SELECT h_name FROM `std_1720_backend_php`.`hashtags` WHERE `h_id` =' . $row['h_id'])->fetch_assoc()['h_name'];
              ?>
              <li class="column__h2">
                <?php echo $hashtag ?>
              </li>
              <?php
              }
            }
          ?>
        </ul>
      </div>


    </div>


  </body>
</html>
